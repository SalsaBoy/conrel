package salsaboy.conrel;

import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.GridLayout;

public class PropertyPane extends JPanel
{
	private JTextField religionName = new JTextField();
	
	public PropertyPane()
	{
		setLayout(new GridLayout(0, 1));
		
		religionName.setText("Religion Name");
		
		add(religionName);
	}
}
