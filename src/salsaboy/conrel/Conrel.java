package salsaboy.conrel;

import javax.swing.*;
import java.awt.BorderLayout;

public class Conrel extends JFrame
{
	private JTabbedPane pane = new JTabbedPane();
	
	private PropertyPane propertyPane = new PropertyPane();
	
	public Conrel()
	{
		setLayout(new BorderLayout());
		
		pane.setTabPlacement(JTabbedPane.LEFT);
		pane.addTab("Properties", propertyPane);
		add(pane);
		
		setSize(640, 480);
		setResizable(true);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public static void main(String[] args)
	{
		new Conrel();
	}
}
